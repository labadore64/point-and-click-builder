﻿using Newtonsoft.Json;
using PointAndClickBuilder.containers;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace PointAndClickBuilder
{
    public partial class Form1 : Form
    {
        DataContainer Data = new DataContainer();

        BindingSource LayerBinding;
        BindingSource BackgroundBinding;
        BindingSource ObjectBinding;

        ElementContainer MovingObj = null;
        bool Down = false;

        Layer SelectedLayer = null;
        ElementContainer SelectedObj = null;

        int x_offset = 0;
        int y_offset = 0;

        string loaded_filename = "";

        public Form1()
        {
            Data.Background = new BackgroundContainer();
            Data.Layers.Add(new Layer());
            Data.Layers[Data.Layers.Count - 1].LayerInit();

            InitializeComponent();
            BindElements();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Data.Background.Draw(e);

            for(int i = 0; i < Data.Layers.Count; i++)
            {
                Data.Layers[i].Draw(e);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog_backgroundImg_FileOk(object sender, CancelEventArgs e)
        {
            pictureBox_backgoundImg.SizeMode = PictureBoxSizeMode.StretchImage;
            Bitmap MyImage = new Bitmap(openFileDialog_backgroundImg.FileName);
            pictureBox_backgoundImg.ClientSize = new Size(pictureBox_backgoundImg.Width, pictureBox_backgoundImg.Height);
            pictureBox_backgoundImg.Image = MyImage;

            pictureBox_backgoundImg.ImageLocation = openFileDialog_backgroundImg.FileName;
            Data.Background.SetImage(openFileDialog_backgroundImg.FileName);
            if (openFileDialog_backgroundImg.FileName.Length > 30)
            {
                textBox_backgoundFilename.Text = "..." + openFileDialog_backgroundImg.FileName.Substring(openFileDialog_backgroundImg.FileName.Length - 30);
            } else
            {
                textBox_backgoundFilename.Text = openFileDialog_backgroundImg.FileName;
            }

            Redraw();
        }

        private void button_backgoundSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog_backgroundImg.ShowDialog();
        }

        private void button_backgoundClear_Click(object sender, EventArgs e)
        {
            Data.Background = new BackgroundContainer("");

            Redraw();
        }

        private void Redraw()
        {
            panel1.Refresh();
        }

        private void BindElements()
        {
            listBox_layers.DataBindings.Clear();
            textBox_backgroundMusic.DataBindings.Clear();
            textBox_backgroundCompletedLocation.DataBindings.Clear();
            checkBox_layerCompiled.DataBindings.Clear();
            checkBox_layerVisible.DataBindings.Clear();
            checkBox_layerLocked.DataBindings.Clear();
            textbox_layerName.DataBindings.Clear();
            textBox_layerEvent.DataBindings.Clear();
            textBox_layerEventFalse.DataBindings.Clear();
            textBox_backgroundLoop.DataBindings.Clear();
            textBox_backgroundRandomSounds.DataBindings.Clear();
            textBox_backgroundTitle.DataBindings.Clear();

            BackgroundBinding = new BindingSource(Data.Background,null);
            textBox_backgroundMusic.DataBindings.Add(
            new Binding("Text", BackgroundBinding,
                "music", false, DataSourceUpdateMode.OnPropertyChanged));
            textBox_backgroundCompletedLocation.DataBindings.Add(
            new Binding("Text", BackgroundBinding,
                "next", false, DataSourceUpdateMode.OnPropertyChanged));
            textBox_backgroundLoop.DataBindings.Add(
            new Binding("Text", BackgroundBinding,
                "sfx_loop", false, DataSourceUpdateMode.OnPropertyChanged));
            textBox_backgroundRandomSounds.DataBindings.Add(
            new Binding("Text", BackgroundBinding,
                "sfx_random", false, DataSourceUpdateMode.OnPropertyChanged));
            textBox_backgroundTitle.DataBindings.Add(
            new Binding("Text", BackgroundBinding,
                "title", false, DataSourceUpdateMode.OnPropertyChanged));

            LayerBinding = new BindingSource(Data.Layers, null);

            listBox_layers.DataSource = LayerBinding;

            checkBox_layerCompiled.DataBindings.Add(
            new Binding("Checked", LayerBinding,
                "compile", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBox_layerVisible.DataBindings.Add(
            new Binding("Checked", LayerBinding,
                "visible", false, DataSourceUpdateMode.OnPropertyChanged));
            checkBox_layerLocked.DataBindings.Add(
            new Binding("Checked", LayerBinding,
                "locked", false, DataSourceUpdateMode.OnPropertyChanged));
            textbox_layerName.DataBindings.Add(
            new Binding("Text", LayerBinding,
                "name", false, DataSourceUpdateMode.OnPropertyChanged));
            textBox_layerEvent.DataBindings.Add(
            new Binding("Text", LayerBinding,
                "eventsTrue", false, DataSourceUpdateMode.OnPropertyChanged));
            textBox_layerEventFalse.DataBindings.Add(
            new Binding("Text", LayerBinding,
                "eventsFalse", false, DataSourceUpdateMode.OnPropertyChanged));

            BindObj();
        }

        private void BindObj()
        {
            if (listBox_layers.SelectedIndex > -1)
            {
                Layer layer = Data.Layers[listBox_layers.SelectedIndex];
                if (layer != null)
                {
                    if (ObjectBinding == null || ObjectBinding.DataSource != layer.Objects)
                    {
                        ObjectBinding = new BindingSource(layer.Objects, null);

                        listBox_objects.DataSource = ObjectBinding;
                    }

                    if (listBox_objects.SelectedIndex > -1) {

                        textBox_objectTitle.DataBindings.Clear();
                        textBox_objectDesc.DataBindings.Clear();
                        textBox_objectXOffset.DataBindings.Clear();
                        textBox_objectYOffset.DataBindings.Clear();
                        textBox_objectWidth.DataBindings.Clear();
                        textBox_objectHeight.DataBindings.Clear();
                        textBox_objectAlpha.DataBindings.Clear();
                        textBox_objectSoundEffect.DataBindings.Clear();
                        textBox_objectNext.DataBindings.Clear();
                        textBox_objectEventActive.DataBindings.Clear();
                        textBox_objectEventInactive.DataBindings.Clear();
                        checkBox_objectCompiled.DataBindings.Clear();
                        checkBox_objectVisible.DataBindings.Clear();
                        checkBox_objectLocked.DataBindings.Clear();
                        checkBox_objectTooltip.DataBindings.Clear();
                        textBox_objectCost.DataBindings.Clear();
                        textBox_objectBlend.DataBindings.Clear();

                        ElementContainer obj = layer.Objects[listBox_objects.SelectedIndex];
                        if (obj != null)
                        {
                            textBox_objectTitle.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "title", false, DataSourceUpdateMode.OnPropertyChanged));
                            textBox_objectDesc.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "desc", false, DataSourceUpdateMode.OnPropertyChanged));
                            textBox_objectXOffset.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "x_offset", false, DataSourceUpdateMode.OnPropertyChanged));
                            textBox_objectYOffset.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "y_offset", false, DataSourceUpdateMode.OnPropertyChanged));
                            textBox_objectWidth.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "width", false, DataSourceUpdateMode.OnPropertyChanged));
                            textBox_objectHeight.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "height", false, DataSourceUpdateMode.OnPropertyChanged));
                            
                            textBox_objectAlpha.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "alpha", false, DataSourceUpdateMode.OnPropertyChanged));
                            textBox_objectSoundEffect.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "sfx", false, DataSourceUpdateMode.OnPropertyChanged));

                            textBox_objectEventActive.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "eventsRequired", false, DataSourceUpdateMode.OnPropertyChanged));
                            textBox_objectEventInactive.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "eventsRequiredFalse", false, DataSourceUpdateMode.OnPropertyChanged));

                            textBox_objectNext.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "next", false, DataSourceUpdateMode.OnPropertyChanged));
                            checkBox_objectCompiled.DataBindings.Add(
                            new Binding("Checked", ObjectBinding,
                                "compile", false, DataSourceUpdateMode.OnPropertyChanged));
                            checkBox_objectVisible.DataBindings.Add(
                            new Binding("Checked", ObjectBinding,
                                "visible", false, DataSourceUpdateMode.OnPropertyChanged));
                            checkBox_objectLocked.DataBindings.Add(
                            new Binding("Checked", ObjectBinding,
                                "locked", false, DataSourceUpdateMode.OnPropertyChanged));
                            checkBox_objectTooltip.DataBindings.Add(
                            new Binding("Checked", ObjectBinding,
                                "tooltip", false, DataSourceUpdateMode.OnPropertyChanged));

                            textBox_objectCost.DataBindings.Add(
                                new Binding("Text", ObjectBinding,
                                    "cost", false, DataSourceUpdateMode.OnPropertyChanged));

                            textBox_objectBlend.DataBindings.Add(
                                new Binding("BackColor", ObjectBinding,
                                    "blend", false, DataSourceUpdateMode.OnPropertyChanged));


                        }
                    }
                }
            }
        }

        private void ResetBindingsNew()
        {
            LayerBinding.ResetBindings(true);
        }

        // functions for the buttons
        private void listboxAdd<T>(ListBox listbox, BindingList<T> list, T newobj)
        {
            if (listbox.SelectedIndex >= 0)
            {
                if (listbox.SelectedIndex == list.Count - 1)
                {
                    list.Add(newobj);
                    if (listbox.SelectedIndex != listbox.Items.Count - 1)
                    {
                        listbox.SelectedIndex++;
                    }
                }
                else
                {
                    list.Insert(listbox.SelectedIndex + 1, newobj);
                    if (listbox.SelectedIndex != listbox.Items.Count - 1)
                    {
                        listbox.SelectedIndex++;
                    }
                }
            }
            else
            {
                list.Add(newobj);
            }
        }

        private void listboxRemove<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.Items.Count > 1)
            {
                if (listbox.SelectedIndex >= 0)
                {
                    list.RemoveAt(listbox.SelectedIndex);
                    if (listbox.SelectedIndex > 0 && listbox.SelectedIndex != list.Count - 1)
                    {
                        listbox.SelectedIndex--;
                    }
                }
            }
        }

        private void listboxMoveUp<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.SelectedIndex > 0)
            {
                T[] characters = new T[2];

                characters[0] = list[listbox.SelectedIndex];
                characters[1] = list[listbox.SelectedIndex - 1];

                list[listbox.SelectedIndex] = characters[1];
                list[listbox.SelectedIndex - 1] = characters[0];
                listbox.SelectedIndex--;
            }
        }

        private void listboxMoveDown<T>(ListBox listbox, BindingList<T> list)
        {
            if (listbox.SelectedIndex < list.Count - 1)
            {
                T[] characters = new T[2];

                characters[0] = list[listbox.SelectedIndex];
                characters[1] = list[listbox.SelectedIndex + 1];

                list[listbox.SelectedIndex] = characters[1];
                list[listbox.SelectedIndex + 1] = characters[0];
                listbox.SelectedIndex++;
            }
        }

        private void button_layerAdd_Click(object sender, EventArgs e)
        {
            Layer layer = new Layer();
            layer.LayerInit();
            listboxAdd(listBox_layers, Data.Layers, layer);
            SetSelectedLayer();
            SetSelectedObject();


            pictureBox_objectPreview.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox_objectPreview.Image = null;

            pictureBox_objectPreview.ImageLocation = "";
        }

        private void button2_layerRemove_Click(object sender, EventArgs e)
        {
            listboxRemove(listBox_layers, Data.Layers);
        }

        private void button_layerMoveUp_Click(object sender, EventArgs e)
        {
            listboxMoveUp(listBox_layers, Data.Layers);
        }

        private void button_layerMoveDown_Click(object sender, EventArgs e)
        {
            listboxMoveDown(listBox_layers, Data.Layers);
        }

        private void textbox_layerName_Leave(object sender, EventArgs e)
        {
            ResetBindingsNew();
        }

        private void UpdateSelectedObj()
        {
            for (int i = 0; i < Data.Layers.Count; i++)
            {
                Data.Layers[i].DeselectAll();
            }


            if (SelectedLayer != null && (listBox_objects.SelectedIndex < SelectedLayer.Objects.Count))
            {
                if (listBox_objects.SelectedIndex > -1 && SelectedLayer.Objects.Count > listBox_objects.SelectedIndex)
                {
                    SelectedLayer.Objects[listBox_objects.SelectedIndex].Selected = true;
                }
            }
        }

        private void listBox_objects_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetSelectedObject();

            if (SelectedLayer != null)
            {
                if (SelectedObj != null)
                {
                    if (File.Exists(SelectedObj.fullpath))
                    {
                        pictureBox_objectPreview.SizeMode = PictureBoxSizeMode.StretchImage;
                        Bitmap MyImage = new Bitmap(SelectedObj.fullpath);
                        pictureBox_objectPreview.ClientSize = new Size(pictureBox_objectPreview.Width, pictureBox_objectPreview.Height);
                        pictureBox_objectPreview.Image = MyImage;

                        pictureBox_objectPreview.ImageLocation = SelectedObj.fullpath;
                        if (SelectedObj.fullpath.Length > 20)
                        {
                            textBox_objectFilename.Text = "..." + SelectedObj.fullpath.Substring(SelectedObj.fullpath.Length - 20);
                        }
                        else
                        {
                            textBox_objectFilename.Text = SelectedObj.fullpath;
                        }
                    } else
                    {
                        pictureBox_objectPreview.Image = null;
                        textBox_objectFilename.Text = "";
                    }
                }
            }

            BindObj();
            Redraw();
        }

        private void checkBox_layerVisible_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedLayer != null)
            {
                //SelectedLayer.visible = checkBox_layerVisible.Checked;
                Redraw();
            }
        }

        private void listBox_layers_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetSelectedLayer();
            listBox_objects_SelectedIndexChanged(sender, e);
            SetSelectedObject();
        }

        private void SetSelectedLayer()
        {
            if (listBox_layers.SelectedIndex > -1 && listBox_layers.SelectedIndex < Data.Layers.Count)
            {
                SelectedLayer = Data.Layers[listBox_layers.SelectedIndex];
            }
            else
            {
                SelectedLayer = null;
            }
        }

        private void SetSelectedObject()
        {
            if (SelectedLayer != null)
            {
                if (listBox_objects.SelectedIndex > -1 && listBox_objects.SelectedIndex < SelectedLayer.Objects.Count)
                {
                    SelectedObj = SelectedLayer.Objects[listBox_objects.SelectedIndex];
                    UpdateSelectedObj();
                }
                else
                {
                    SelectedObj = null;
                }
            }
        }

        private void textBox_objectTitle_Leave(object sender, EventArgs e)
        {
            ObjectBinding.ResetBindings(true);
        }

        private void checkBox_objectVisible_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedObj != null)
            {
                //SelectedObj.visible = checkBox_objectVisible.Checked;
                Redraw();
            }
        }

        private void checkBox_layerLocked_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedLayer != null)
            {
                //SelectedLayer.locked = checkBox_layerLocked.Checked;
                Redraw();
            }
        }

        private void checkBox_objectLocked_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedObj != null)
            {
                //SelectedObj.locked = checkBox_objectLocked.Checked;
                Redraw();
            }
        }

        private void button_objectsAdd_Click(object sender, EventArgs e)
        {
            if (listBox_layers.SelectedIndex > -1)
            {
                Layer layer = Data.Layers[listBox_layers.SelectedIndex];
                ElementContainer container = new ElementContainer();
                listboxAdd(listBox_objects, layer.Objects, container);
                container.Parent = layer;

            }
        }

        private void button_objectsRemove_Click(object sender, EventArgs e)
        {
            if (listBox_layers.SelectedIndex > -1)
            {
                Layer layer = Data.Layers[listBox_layers.SelectedIndex];
                listboxRemove(listBox_objects, layer.Objects);
            }
        }

        private void button_objectsMoveUp_Click(object sender, EventArgs e)
        {
            if (listBox_layers.SelectedIndex > -1)
            {
                Layer layer = Data.Layers[listBox_layers.SelectedIndex];
                listboxMoveUp(listBox_objects, layer.Objects);
            }
        }

        private void button_objectsMoveDown_Click(object sender, EventArgs e)
        {
            if (listBox_layers.SelectedIndex > -1)
            {
                Layer layer = Data.Layers[listBox_layers.SelectedIndex];
                listboxMoveDown(listBox_objects, layer.Objects);
            }
        }

        private void button_objectSelect_Click(object sender, EventArgs e)
        {
            openFileDialog_objImage.ShowDialog();
        }

        private void openFileDialog_objImage_FileOk(object sender, CancelEventArgs e)
        {

            if (SelectedObj != null)
            {
                pictureBox_objectPreview.SizeMode = PictureBoxSizeMode.StretchImage;
                Bitmap MyImage = new Bitmap(openFileDialog_objImage.FileName);
                pictureBox_objectPreview.ClientSize = new Size(pictureBox_objectPreview.Width, pictureBox_objectPreview.Height);
                pictureBox_objectPreview.Image = MyImage;

                pictureBox_objectPreview.ImageLocation = openFileDialog_objImage.FileName;
                SelectedObj.SetImage(openFileDialog_objImage.FileName);
                if (openFileDialog_objImage.FileName.Length > 20)
                {
                    textBox_objectFilename.Text = "..." + openFileDialog_objImage.FileName.Substring(openFileDialog_objImage.FileName.Length - 20);
                }
                else
                {
                    textBox_objectFilename.Text = openFileDialog_objImage.FileName;
                }
                Redraw();
            }

        }

        private void textBox_objectBlend_Click(object sender, EventArgs e)
        {
            colorDialog.ShowDialog();
            textBox_objectBlend.BackColor = colorDialog.Color;

            if (SelectedObj != null)
            {
                SelectedObj.blend = colorDialog.Color;
            }
        }

        private void button_objectClear_Click(object sender, EventArgs e)
        {

            if (SelectedObj != null)
            {
                SelectedObj.ClearImage();
                Redraw();
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            Down = true;

            bool cando = true;

            {
                for (int i = 0; i < Data.Layers.Count; i++)
                {
                    for (int j = Data.Layers[i].Objects.Count - 1; j >= 0; j--)
                    {
                        ElementContainer obj = Data.Layers[i].Objects[j];
                        if (obj != null && !obj.locked && !obj.Parent.locked)
                        {
                            bool answer = false;
                            if (obj.Image.Bitmap != null)
                            {
                                answer = e.X > (obj.x-obj.x_offset) && e.X < (obj.x-obj.x_offset) + obj.Image.Bitmap.Width
                                    && e.Y > (obj.y-obj.y_offset) && e.Y < (obj.y - obj.y_offset) + obj.Image.Bitmap.Height;
                            }
                            else
                            {
                                answer = e.X > (obj.x - obj.x_offset) && e.X < (obj.x - obj.x_offset) + obj.width
                                    && e.Y > (obj.y - obj.y_offset) && e.Y < (obj.y - obj.y_offset) + obj.height;
                            }
                            if (answer)
                            {
                                // so it doesn't grab alpha pixels
                                if (obj.Image.Bitmap == null || obj.Image.Bitmap.GetPixel(e.X - (obj.x-obj.x_offset), e.Y - (obj.y - obj.y_offset)).A > 51)
                                {
                                    MovingObj = obj;
                                    listBox_layers.SelectedIndex = i;
                                    listBox_objects.SelectedIndex = j;
                                    SetSelectedLayer();
                                    SetSelectedObject();

                                    x_offset = e.X - obj.x;
                                    y_offset = e.Y - obj.y;

                                    cando = false;

                                    if (File.Exists(obj.fullpath))
                                    {
                                        
                                        pictureBox_objectPreview.SizeMode = PictureBoxSizeMode.StretchImage;
                                        Bitmap MyImage = new Bitmap(obj.fullpath);
                                        pictureBox_objectPreview.ClientSize = new Size(pictureBox_objectPreview.Width, pictureBox_objectPreview.Height);
                                        pictureBox_objectPreview.Image = MyImage;

                                        pictureBox_objectPreview.ImageLocation = obj.fullpath;
                                        if (obj.fullpath.Length > 20)
                                        {
                                            textBox_objectFilename.Text = "..." + obj.fullpath.Substring(obj.fullpath.Length - 20);
                                        }
                                        else
                                        {
                                            textBox_objectFilename.Text = obj.fullpath;
                                        }
                                    }

                                    break;
                                }
                            }

                        }
                    }
                }
            }

            if (cando)
            {
                listBox_objects.SelectedIndex = -1;
            }

            SetSelectedObject();
            UpdateSelectedObj();
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            Down = false;
            MovingObj = null;
            Redraw();
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (Down)
            {
                if (MovingObj != null)
                {
                    MovingObj.x = e.X- x_offset;
                    MovingObj.y = e.Y- y_offset;
                    Redraw();
                }
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            for (var i = 0; i < Data.Layers.Count; i++)
            {
                Data.Layers[i].Draw(e);
            }
        }

        private void saveFile(string path)
        {
            string output = JsonConvert.SerializeObject(new JsonOutput(Data));
            File.WriteAllText(path, output);
        }

        private void button_IOCompile_Click(object sender, EventArgs e)
        {
            saveFileDialog_export.ShowDialog();
        }

        private void saveFileDialog_export_FileOk(object sender, CancelEventArgs e)
        {
            saveFile(saveFileDialog_export.FileName);
        }

        private void textBox_objectXOffset_Leave(object sender, EventArgs e)
        {
            Redraw();
        }

        private void textBox_objectYOffset_Leave(object sender, EventArgs e)
        {
            Redraw();
        }

        private void textBox_objectWidth_Leave(object sender, EventArgs e)
        {
            Redraw();
        }

        private void textBox_objectHeight_Leave(object sender, EventArgs e)
        {
            Redraw();
        }

        private void textBox_objectAlpha_Leave(object sender, EventArgs e)
        {
            Redraw();
        }

        private void textBox_objectBlend_Leave(object sender, EventArgs e)
        {
            Redraw();
        }

        private void saveFileDialog_save_FileOk(object sender, CancelEventArgs e)
        {
            string output = JsonConvert.SerializeObject(Data);
            File.WriteAllText(saveFileDialog_save.FileName, output);
            loaded_filename = saveFileDialog_save.FileName;
            Text = "P&C Builder - ..." + loaded_filename.Substring(Math.Max(loaded_filename.Length - 20, 0));
        }

        private void button_IOSave_Click(object sender, EventArgs e)
        {
            saveFileDialog_save.ShowDialog();

        }

        private void button_IOLoad_Click(object sender, EventArgs e)
        {
            openFileDialog_load.ShowDialog();
        }

        private void openFileDialog_load_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                Layer.LayerCounter = 0;
                ElementContainer.ElementCounter = 0;
                string input = File.ReadAllText(openFileDialog_load.FileName);
                Data = JsonConvert.DeserializeObject<DataContainer>(input);
                // sets proper parents for layers
                if (Data.Layers.Count > 0)
                {
                    Data.SetLayersParent();
                    Data.SetBackground();
                    UpdateSelectedObj();
                    BindElements();
                    loaded_filename = Data.Background.Image.Path;

                    pictureBox_backgoundImg.SizeMode = PictureBoxSizeMode.StretchImage;
                    if (Data.Background.Image.Bitmap != null)
                    {
                        Bitmap MyImage = new Bitmap(Data.Background.Image.Bitmap);
                        pictureBox_backgoundImg.ClientSize = new Size(pictureBox_backgoundImg.Width, pictureBox_backgoundImg.Height);
                        pictureBox_backgoundImg.Image = MyImage;

                    }

                    if (loaded_filename.Length > 30)
                    {
                        textBox_backgoundFilename.Text = "..." + loaded_filename.Substring(loaded_filename.Length - 30);
                    }
                    else
                    {
                        textBox_backgoundFilename.Text = loaded_filename;
                    }


                    Text = "P&C Builder - ..." + loaded_filename.Substring(Math.Max(loaded_filename.Length - 20, 0));
                    Redraw();
                } else
                {
                    throw new FileLoadException();
                }
            } catch
            {
                Text = "P&C Builder";
                MessageBox.Show("That's an invalid file!");
                Data = new DataContainer();
                Data.SetLayersParent();
                Data.SetBackground();
                Layer layer = new Layer();
                layer.LayerInit();
                Data.Layers.Add(layer);
                BindElements();
                Redraw();
            }
        }
    }
}
