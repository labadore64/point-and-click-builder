﻿using PointAndClickBuilder.containers;
using System.Collections.Generic;
using System.Drawing;

namespace PointAndClickBuilder
{
    public class JsonOutputElementContainer
    {
        public int x;
        public int y;
        public int width;
        public int height;
        public int x_offset;
        public int y_offset;
        public int alpha;
        public int cost;
        public string blend = "";
        public string title = "";
        public string desc = "";
        public string next = "";
        public string sfx = "";
        public string sprite = "";
        public bool show = true;
        public List<string> required_events = new List<string>();
        public List<string> required_events_false = new List<string>();

        string colorToString(Color color)
        {
            return color.B.ToString("X2") + color.G.ToString("X2") + color.R.ToString("X2");
        }

        public JsonOutputElementContainer(ElementContainer obj)
        {
            x = obj.x;
            y = obj.y;
            width = obj.width;
            height = obj.height;
            x_offset = obj.x_offset;
            y_offset = obj.y_offset;
            alpha = obj.alpha;
            cost = obj.cost;
            title = obj.title;
            desc = obj.desc;
            next = obj.next;
            sfx = obj.sfx;
            sprite = obj.path;
            show = obj.tooltip;
            blend = colorToString(obj.blend);
            string[] req;
            string[] nonreq;

            if (obj.Parent != null)
            {
                req = obj.Parent.eventsTrue.Split(',');
                nonreq = obj.Parent.eventsFalse.Split(',');

                for (int i = 0; i < req.Length; i++)
                {
                    if (!required_events.Contains(req[i]) && req[i].Length > 0)
                    {
                        required_events.Add(req[i]);
                    }
                }
                for (int i = 0; i < nonreq.Length; i++)
                {
                    if (!required_events_false.Contains(nonreq[i]) && nonreq[i].Length > 0)
                    {
                        required_events_false.Add(nonreq[i]);
                    }
                }
            }

            req = obj.eventsRequired.Split(',');
            nonreq = obj.eventsRequiredFalse.Split(',');

            for (int i = 0; i < req.Length; i++)
            {
                if (!required_events.Contains(req[i]) && req[i].Length > 0)
                {
                    required_events.Add(req[i]);
                }
            }
            for (int i = 0; i < nonreq.Length; i++)
            {
                if (!required_events_false.Contains(nonreq[i]) && nonreq[i].Length > 0)
                {
                    required_events_false.Add(nonreq[i]);
                }
            }

        }
    }
}
