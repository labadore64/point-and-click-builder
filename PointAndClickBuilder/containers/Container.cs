﻿
using System.Windows.Forms;

namespace PointAndClickBuilder.containers
{
    public interface Container
    {
        void Draw(PaintEventArgs e);
        ImageContainer Image { get; }
    }
}
