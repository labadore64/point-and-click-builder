﻿
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.IO;
using System.Xml.Serialization;

namespace PointAndClickBuilder.containers
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ImageContainer
    {
        // image used for the element
        public Bitmap Bitmap { get; protected set; }

        string internalPath = "";
        [JsonProperty]
        public string Path
        {
            get
            {
                return internalPath;
            }
            set
            {
                internalPath = value;
                SetImg(value);
            }
        }

        // variables about image values
        [JsonProperty]
        public int x { get; set; } = 20;
        [JsonProperty]
        public int y { get; set; } = 20;
        [JsonProperty]
        public int width { get; set; } = 80;
        [JsonProperty]
        public int height { get; set; } = 80;
        [JsonProperty]
        public int x_offset { get; set; } = 0;
        [JsonProperty]
        public int y_offset { get; set; } = 0;
        [JsonProperty]
        public int alpha { get; set; } = 100;
        public Color blend { get; set; }

        public ImageContainer()
        {
            blend = Color.Red;
            Bitmap = null;
            Path = "";
        }

        public ImageContainer(string path)
        {
            blend = Color.Red;
        }

        public void ClearImg()
        {
            Bitmap = null;
        }

        public void SetImg(string path)
        {
            if (File.Exists(path))
            {
                Bitmap test = new Bitmap(path);
                Bitmap = new Bitmap(test, new Size(test.Width, test.Height));
            } else
            {
                ClearImg();
            }
        }

    }
}
