﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace PointAndClickBuilder.containers
{
    public class Layer
    {
        public BindingList<ElementContainer> Objects = new BindingList<ElementContainer>();

        public bool visible { get; set; } = true;
        public bool locked { get; set; } = false;
        public bool compile { get; set; } = true;
        public string name { get; set; } = "";

        string eventsTrueInternal = "";


        public string eventsTrue
        {
            get
            {
                return eventsTrueInternal.Replace(System.Environment.NewLine, ",");
            }
            set
            {
                eventsTrueInternal = value;
            }
        }

        string eventsFalseInternal = "";


        public string eventsFalse
        {
            get
            {
                return eventsFalseInternal.Replace(System.Environment.NewLine, ",");
            }
            set
            {
                eventsFalseInternal = value;
            }
        }

        public static int LayerCounter = 0;

        public Layer()
        {
            LayerCounter++;
            name = "Layer" + LayerCounter;
        }

        public void LayerInit()
        {
            Objects.Add(new ElementContainer());

            Objects[0].Parent = this;
        }

        public override string ToString()
        {
            return name;
        }

        public void Draw(PaintEventArgs e)
        {
            if (visible)
            {
                for(int i = 0; i < Objects.Count; i++)
                {
                    Objects[i].Draw(e);
                }
            }
        }

        public void DeselectAll()
        {
            for(int i = 0; i < Objects.Count; i++)
            {
                Objects[i].Selected = false;
            }
        }
    }
}
