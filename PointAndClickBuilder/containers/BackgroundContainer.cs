﻿

using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace PointAndClickBuilder.containers
{
    public class BackgroundContainer : Container, INotifyPropertyChanged
    {
        // image used for the element
        public ImageContainer Image { get; protected set; }

        // properties for the output

        public string bg { get; protected set; } = "";
        public string next { get; set; } = "";
        public string music { get; set; } = "";
        public string title { get; set; } = "";

        public string sfx_loop { get; set; } = "";

        string sfx_randomInternal = "";
        public string sfx_random
        {
            get
            {
                return sfx_randomInternal.Replace(System.Environment.NewLine, ",");
            }
            set
            {
                sfx_randomInternal = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void SetImage(string path)
        {
            if (File.Exists(path))
            {
                Image.Path = path;
                SetBg();
            }
            else
            {
                Image.ClearImg();
            }
        }

        public void SetBg()
        {
            if (Image != null)
            {
                bg = Path.GetFileNameWithoutExtension(Image.Path).Replace("bg_", "");
            }
        }

        public BackgroundContainer()
        {
            Image = new ImageContainer();
        }

        public BackgroundContainer(string path)
        {
            SetImage(path);
        }

        public void Draw(PaintEventArgs e)
        {
            if (Image.Bitmap != null)
            {
                e.Graphics.DrawImage(Image.Bitmap, 0, 0);
            }
        }
    }
}
