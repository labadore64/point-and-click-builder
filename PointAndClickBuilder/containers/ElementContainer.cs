﻿
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace PointAndClickBuilder.containers
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ElementContainer : Container, INotifyPropertyChanged
    {
        // image used for the element
        [JsonProperty]
        public ImageContainer Image { get; set; }


        public static int ElementCounter = 0;


        public bool Selected { get; set; } = true;

        [JsonProperty]
        public string path { get; set; } = "";
        [JsonProperty]
        public int cost { get; set; }
        [JsonProperty]
        public string fullpath { get; protected set; } = "";

        // variables about image values
        [JsonProperty]
        public int x
        {
            get
            {
                if (Image != null)
                {
                    return Image.x;
                } else
                {
                    return 0;
                }
            }
            set
            {
                if (Image != null)
                {
                    Image.x = value;
                }
            }
        }
        [JsonProperty]
        public int y
        {
            get
            {
                if (Image != null)
                {
                    return Image.y;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (Image != null)
                {
                    Image.y = value;
                }
            }
        }
        [JsonProperty]
        public int width
        {
            get
            {
                if (Image != null)
                {
                    return Image.width;
                }
                else
                {
                    return 1;
                }
            }
            set
            {
                if (Image != null)
                {
                    Image.width = value;
                }
            }
        }
        [JsonProperty]
        public int height
        {
            get
            {
                if (Image != null)
                {
                    return Image.height;
                }
                else
                {
                    return 1;
                }
            }
            set
            {
                if (Image != null)
                {
                    Image.height = value;
                }
            }
        }
        [JsonProperty]
        public int x_offset
        {
            get
            {
                if (Image != null)
                {
                    return Image.x_offset;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (Image != null)
                {
                    Image.x_offset = value;
                }
            }
        }
        [JsonProperty]
        public int y_offset
        {
            get
            {
                if (Image != null)
                {
                    return Image.y_offset;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (Image != null)
                {
                    Image.y_offset = value;
                }
            }
        }

        [JsonProperty]
        public int alpha
        {
            get
            {
                if (Image != null)
                {
                    return Image.alpha;
                }
                else
                {
                    return 100;
                }
            }
            set
            {
                if (Image != null)
                {
                    Image.alpha = value;
                }
            }
        }


        public Color blend
        {
            get
            {
                if (Image != null)
                {
                    return Image.blend;
                }
                else
                {
                    return Color.Red;
                }
            }
            set
            {
                if (Image != null)
                {
                    R = value.R;
                    G = value.G;
                    B = value.B;
                    Image.blend = value;
                }
            }
        }

        [JsonProperty]
        public int R
        {
            get; protected set;
        }

        [JsonProperty]
        public int G
        {
            get; protected set;
        }

        [JsonProperty]
        public int B
        {
            get; protected set;
        }

        public void SetColor()
        {
            blend = Color.FromArgb(255,R,G,B);
        }

        // properties for managing in the editor

        [JsonProperty]
        public bool visible { get; set; } = true;
        [JsonProperty]
        public bool locked { get; set; } = false;
        [JsonProperty]
        public bool compile { get; set; } = true;
        [JsonProperty]
        public bool tooltip { get; set; } = true;

        // other properties

        [JsonProperty]
        public string title { get; set; } = "";

        [JsonProperty]
        public string desc { get; set; } = "";
        [JsonProperty]
        public string next { get; set; } = "";
        [JsonProperty]
        public string sfx { get; set; } = "";

        string eventsRequiredInternal = "";

        [JsonProperty]
        public string eventsRequired
        {
            get
            {
                return eventsRequiredInternal.Replace(Environment.NewLine, ",");
            }
            set
            {
                eventsRequiredInternal = value;
            }
        }

        string eventsRequiredFalseInternal = "";

        [JsonProperty]
        public string eventsRequiredFalse
        {
            get
            {
                return eventsRequiredFalseInternal.Replace(System.Environment.NewLine, ",");
            }
            set
            {
                eventsRequiredFalseInternal = value;
            }
        }

        public Layer Parent { get; set; }

        public ElementContainer()
        {
            Image = new ImageContainer();
            SetImage("");
            ElementCounter++;
            title = "Obj" + ElementCounter;
        }

        public ElementContainer(string path)
        {
            if (File.Exists(path))
            {
                Image = new ImageContainer(path);
            }
            else
            {
                Image = new ImageContainer();
            }
            ElementCounter++;
            title = "Obj" + ElementCounter;
        }

        public void SetImage(string path)
        {
            if (File.Exists(path))
            {
                Image.Path = path;
                this.path = Path.GetFileNameWithoutExtension(path);
                fullpath = path;
            }
            else
            {
                Image.ClearImg();
            }
        }

        public void ClearImage()
        {
            Image.ClearImg();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Draw(PaintEventArgs e)
        {
            if (visible)
            {
                int xx = x - x_offset;
                int yy = y - y_offset;
                if (Selected)
                {
                    // Create pen.
                    Color color = Color.Red;
                    if (locked || (Parent != null && Parent.locked))
                    {
                        color = Color.FromArgb(255, 51, 51, 51);
                    }


                    Pen blackPen = new Pen(color, 5);
                    
                    Rectangle rect;
                    // Create rectangle.
                    if (Image.Bitmap != null)
                    {
                        rect = new Rectangle(xx, yy, Image.Bitmap.Width, Image.Bitmap.Height);
                    } else
                    {
                        rect = new Rectangle(xx, yy, width, height);
                    }

                    // Draw rectangle to screen.
                    e.Graphics.DrawRectangle(blackPen, rect);
                }

                if (Image.Bitmap != null)
                {
                    e.Graphics.DrawImage(Image.Bitmap, xx, yy);
                    if (Selected)
                    {

                        Pen overlayPen = new Pen(Color.Aqua, 3);
                        Rectangle rect2 = new Rectangle(xx + x_offset, yy + y_offset, width, height);
                        e.Graphics.DrawRectangle(overlayPen, rect2);
                    }
                }
            }
        }

        public override string ToString()
        {
            return title;
        }
    }
}
