﻿using PointAndClickBuilder.containers;
using System.Collections.Generic;
using System.ComponentModel;

namespace PointAndClickBuilder
{
    public class JsonOutput
    {
        public List<JsonOutputElementContainer> objects = new List<JsonOutputElementContainer>();
        public string type = "point";
        public string next = "";
        public string bg = "";
        public string music = "";
        public string title = "";
        public List<string> sfx_random = new List<string>();
        public string sfx_loop = "";

        public JsonOutput(DataContainer data)
        {
            for(int i = 0; i < data.Layers.Count; i++)
            {
                if (data.Layers[i].compile)
                {

                    for (int j = 0; j < data.Layers[i].Objects.Count; j++)
                    {
                        if (data.Layers[i].Objects[j].compile) {
                            objects.Add(new JsonOutputElementContainer(data.Layers[i].Objects[j]));
                        }
                    }
                }
            }

            title = data.Background.title;
            next = data.Background.next;
            bg = data.Background.bg;
            music = data.Background.music;
            sfx_loop = data.Background.sfx_loop;
            sfx_random = new List<string>(data.Background.sfx_random.Split(','));
        }
    }
}
