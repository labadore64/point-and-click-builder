﻿using PointAndClickBuilder.containers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointAndClickBuilder
{
    public class DataContainer
    {
        public BindingList<Layer> Layers = new BindingList<Layer>();
        public BackgroundContainer Background = new BackgroundContainer();

        public void SetLayersParent()
        {
            for(int i = 0; i < Layers.Count; i++)
            {
                Layer layer = Layers[i];
                if(layer != null)
                {
                    for(int j = 0; j < layer.Objects.Count; j++)
                    {
                        if(layer.Objects[j] != null)
                        {
                            layer.Objects[j].Parent = layer;
                            layer.Objects[j].eventsRequired = layer.Objects[j].eventsRequired.Replace(",",Environment.NewLine);
                            layer.Objects[j].eventsRequiredFalse = layer.Objects[j].eventsRequiredFalse.Replace(",",Environment.NewLine);
                            layer.Objects[j].SetColor();
                        }
                    }
                }
            }
        }

        public void SetBackground()
        {
            if(Background != null)
            {
                Background.SetBg();
                Background.sfx_random = Background.sfx_random.Replace(",",Environment.NewLine);
            }
        }
    }
}
