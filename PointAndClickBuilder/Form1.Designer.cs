﻿namespace PointAndClickBuilder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_backgroundLoop = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox_backgroundRandomSounds = new System.Windows.Forms.TextBox();
            this.textBox_backgroundCompletedLocation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_backgroundMusic = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_backgoundClear = new System.Windows.Forms.Button();
            this.button_backgoundSelectFile = new System.Windows.Forms.Button();
            this.pictureBox_backgoundImg = new System.Windows.Forms.PictureBox();
            this.textBox_backgoundFilename = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog_backgroundImg = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textbox_layerName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox_objectCost = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBox_objectCompiled = new System.Windows.Forms.CheckBox();
            this.textBox_objectEventInactive = new System.Windows.Forms.TextBox();
            this.checkBox_objectLocked = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.checkBox_objectVisible = new System.Windows.Forms.CheckBox();
            this.textBox_objectEventActive = new System.Windows.Forms.TextBox();
            this.button_objectsMoveDown = new System.Windows.Forms.Button();
            this.button_objectsMoveUp = new System.Windows.Forms.Button();
            this.button_objectsRemove = new System.Windows.Forms.Button();
            this.button_objectsAdd = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.listBox_objects = new System.Windows.Forms.ListBox();
            this.textBox_objectNext = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox_objectSoundEffect = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox_objectAlpha = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox_objectBlend = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox_objectWidth = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_objectHeight = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox_objectXOffset = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_objectYOffset = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_objectDesc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_objectTitle = new System.Windows.Forms.TextBox();
            this.textBox_objectFilename = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button_objectClear = new System.Windows.Forms.Button();
            this.button_objectSelect = new System.Windows.Forms.Button();
            this.pictureBox_objectPreview = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox_layerCompiled = new System.Windows.Forms.CheckBox();
            this.checkBox_layerLocked = new System.Windows.Forms.CheckBox();
            this.checkBox_layerVisible = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_layerEventFalse = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_layerEvent = new System.Windows.Forms.TextBox();
            this.button_layerMoveDown = new System.Windows.Forms.Button();
            this.button_layerMoveUp = new System.Windows.Forms.Button();
            this.button2_layerRemove = new System.Windows.Forms.Button();
            this.button_layerAdd = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox_layers = new System.Windows.Forms.ListBox();
            this.openFileDialog_objImage = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.button_IOSave = new System.Windows.Forms.Button();
            this.button_IOLoad = new System.Windows.Forms.Button();
            this.button_IOCompile = new System.Windows.Forms.Button();
            this.saveFileDialog_export = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialog_save = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog_load = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new PointAndClickBuilder.NewPanel();
            this.checkBox_objectTooltip = new System.Windows.Forms.CheckBox();
            this.textBox_backgroundTitle = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_backgoundImg)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_objectPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_backgroundTitle);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.textBox_backgroundLoop);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.textBox_backgroundRandomSounds);
            this.groupBox1.Controls.Add(this.textBox_backgroundCompletedLocation);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox_backgroundMusic);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button_backgoundClear);
            this.groupBox1.Controls.Add(this.button_backgoundSelectFile);
            this.groupBox1.Controls.Add(this.pictureBox_backgoundImg);
            this.groupBox1.Controls.Add(this.textBox_backgoundFilename);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(815, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(589, 181);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Background";
            // 
            // textBox_backgroundLoop
            // 
            this.textBox_backgroundLoop.Location = new System.Drawing.Point(472, 35);
            this.textBox_backgroundLoop.Name = "textBox_backgroundLoop";
            this.textBox_backgroundLoop.Size = new System.Drawing.Size(80, 20);
            this.textBox_backgroundLoop.TabIndex = 41;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(469, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(65, 13);
            this.label25.TabIndex = 40;
            this.label25.Text = "Loop Sound";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(469, 59);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "Random Sounds";
            // 
            // textBox_backgroundRandomSounds
            // 
            this.textBox_backgroundRandomSounds.Location = new System.Drawing.Point(472, 78);
            this.textBox_backgroundRandomSounds.Multiline = true;
            this.textBox_backgroundRandomSounds.Name = "textBox_backgroundRandomSounds";
            this.textBox_backgroundRandomSounds.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_backgroundRandomSounds.Size = new System.Drawing.Size(100, 86);
            this.textBox_backgroundRandomSounds.TabIndex = 9;
            // 
            // textBox_backgroundCompletedLocation
            // 
            this.textBox_backgroundCompletedLocation.Location = new System.Drawing.Point(319, 130);
            this.textBox_backgroundCompletedLocation.Name = "textBox_backgroundCompletedLocation";
            this.textBox_backgroundCompletedLocation.Size = new System.Drawing.Size(137, 20);
            this.textBox_backgroundCompletedLocation.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(212, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Completed Location";
            // 
            // textBox_backgroundMusic
            // 
            this.textBox_backgroundMusic.Location = new System.Drawing.Point(267, 104);
            this.textBox_backgroundMusic.Name = "textBox_backgroundMusic";
            this.textBox_backgroundMusic.Size = new System.Drawing.Size(189, 20);
            this.textBox_backgroundMusic.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Music";
            // 
            // button_backgoundClear
            // 
            this.button_backgoundClear.Location = new System.Drawing.Point(360, 19);
            this.button_backgoundClear.Name = "button_backgoundClear";
            this.button_backgoundClear.Size = new System.Drawing.Size(75, 23);
            this.button_backgoundClear.TabIndex = 4;
            this.button_backgoundClear.Text = "Clear";
            this.button_backgoundClear.UseVisualStyleBackColor = true;
            this.button_backgoundClear.Click += new System.EventHandler(this.button_backgoundClear_Click);
            // 
            // button_backgoundSelectFile
            // 
            this.button_backgoundSelectFile.Location = new System.Drawing.Point(238, 19);
            this.button_backgoundSelectFile.Name = "button_backgoundSelectFile";
            this.button_backgoundSelectFile.Size = new System.Drawing.Size(75, 23);
            this.button_backgoundSelectFile.TabIndex = 3;
            this.button_backgoundSelectFile.Text = "Select File";
            this.button_backgoundSelectFile.UseVisualStyleBackColor = true;
            this.button_backgoundSelectFile.Click += new System.EventHandler(this.button_backgoundSelectFile_Click);
            // 
            // pictureBox_backgoundImg
            // 
            this.pictureBox_backgoundImg.Location = new System.Drawing.Point(6, 16);
            this.pictureBox_backgoundImg.Name = "pictureBox_backgoundImg";
            this.pictureBox_backgoundImg.Size = new System.Drawing.Size(200, 150);
            this.pictureBox_backgoundImg.TabIndex = 2;
            this.pictureBox_backgoundImg.TabStop = false;
            // 
            // textBox_backgoundFilename
            // 
            this.textBox_backgoundFilename.Location = new System.Drawing.Point(267, 52);
            this.textBox_backgoundFilename.Name = "textBox_backgoundFilename";
            this.textBox_backgoundFilename.ReadOnly = true;
            this.textBox_backgoundFilename.Size = new System.Drawing.Size(189, 20);
            this.textBox_backgoundFilename.TabIndex = 1;
            this.textBox_backgoundFilename.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(212, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Filename";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // openFileDialog_backgroundImg
            // 
            this.openFileDialog_backgroundImg.DefaultExt = "*.png";
            this.openFileDialog_backgroundImg.Filter = "PNG Files|*.png";
            this.openFileDialog_backgroundImg.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_backgroundImg_FileOk);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textbox_layerName);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.checkBox_layerCompiled);
            this.groupBox2.Controls.Add(this.checkBox_layerLocked);
            this.groupBox2.Controls.Add(this.checkBox_layerVisible);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox_layerEventFalse);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox_layerEvent);
            this.groupBox2.Controls.Add(this.button_layerMoveDown);
            this.groupBox2.Controls.Add(this.button_layerMoveUp);
            this.groupBox2.Controls.Add(this.button2_layerRemove);
            this.groupBox2.Controls.Add(this.button_layerAdd);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.listBox_layers);
            this.groupBox2.Location = new System.Drawing.Point(821, 199);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(583, 490);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contents";
            // 
            // textbox_layerName
            // 
            this.textbox_layerName.Location = new System.Drawing.Point(169, 32);
            this.textbox_layerName.Name = "textbox_layerName";
            this.textbox_layerName.Size = new System.Drawing.Size(80, 20);
            this.textbox_layerName.TabIndex = 39;
            this.textbox_layerName.Leave += new System.EventHandler(this.textbox_layerName_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(128, 35);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 38;
            this.label20.Text = "Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox_objectTooltip);
            this.groupBox3.Controls.Add(this.textBox_objectCost);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.checkBox_objectCompiled);
            this.groupBox3.Controls.Add(this.textBox_objectEventInactive);
            this.groupBox3.Controls.Add(this.checkBox_objectLocked);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.checkBox_objectVisible);
            this.groupBox3.Controls.Add(this.textBox_objectEventActive);
            this.groupBox3.Controls.Add(this.button_objectsMoveDown);
            this.groupBox3.Controls.Add(this.button_objectsMoveUp);
            this.groupBox3.Controls.Add(this.button_objectsRemove);
            this.groupBox3.Controls.Add(this.button_objectsAdd);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.listBox_objects);
            this.groupBox3.Controls.Add(this.textBox_objectNext);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.textBox_objectSoundEffect);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textBox_objectAlpha);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBox_objectBlend);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBox_objectWidth);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textBox_objectHeight);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBox_objectXOffset);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.textBox_objectYOffset);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textBox_objectDesc);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.textBox_objectTitle);
            this.groupBox3.Controls.Add(this.textBox_objectFilename);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.button_objectClear);
            this.groupBox3.Controls.Add(this.button_objectSelect);
            this.groupBox3.Controls.Add(this.pictureBox_objectPreview);
            this.groupBox3.Location = new System.Drawing.Point(132, 124);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(445, 358);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Objects";
            // 
            // textBox_objectCost
            // 
            this.textBox_objectCost.Location = new System.Drawing.Point(389, 276);
            this.textBox_objectCost.Name = "textBox_objectCost";
            this.textBox_objectCost.Size = new System.Drawing.Size(49, 20);
            this.textBox_objectCost.TabIndex = 45;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(355, 279);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(28, 13);
            this.label23.TabIndex = 44;
            this.label23.Text = "Cost";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(246, 279);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 43;
            this.label21.Text = "Events Inactive";
            // 
            // checkBox_objectCompiled
            // 
            this.checkBox_objectCompiled.AutoSize = true;
            this.checkBox_objectCompiled.Location = new System.Drawing.Point(259, 221);
            this.checkBox_objectCompiled.Name = "checkBox_objectCompiled";
            this.checkBox_objectCompiled.Size = new System.Drawing.Size(69, 17);
            this.checkBox_objectCompiled.TabIndex = 40;
            this.checkBox_objectCompiled.Text = "Compiled";
            this.checkBox_objectCompiled.UseVisualStyleBackColor = true;
            // 
            // textBox_objectEventInactive
            // 
            this.textBox_objectEventInactive.Location = new System.Drawing.Point(249, 295);
            this.textBox_objectEventInactive.Multiline = true;
            this.textBox_objectEventInactive.Name = "textBox_objectEventInactive";
            this.textBox_objectEventInactive.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_objectEventInactive.Size = new System.Drawing.Size(100, 57);
            this.textBox_objectEventInactive.TabIndex = 42;
            // 
            // checkBox_objectLocked
            // 
            this.checkBox_objectLocked.AutoSize = true;
            this.checkBox_objectLocked.Location = new System.Drawing.Point(191, 222);
            this.checkBox_objectLocked.Name = "checkBox_objectLocked";
            this.checkBox_objectLocked.Size = new System.Drawing.Size(62, 17);
            this.checkBox_objectLocked.TabIndex = 40;
            this.checkBox_objectLocked.Text = "Locked";
            this.checkBox_objectLocked.UseVisualStyleBackColor = true;
            this.checkBox_objectLocked.CheckStateChanged += new System.EventHandler(this.checkBox_objectLocked_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(131, 279);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 41;
            this.label22.Text = "Events Active";
            // 
            // checkBox_objectVisible
            // 
            this.checkBox_objectVisible.AutoSize = true;
            this.checkBox_objectVisible.Location = new System.Drawing.Point(134, 223);
            this.checkBox_objectVisible.Name = "checkBox_objectVisible";
            this.checkBox_objectVisible.Size = new System.Drawing.Size(56, 17);
            this.checkBox_objectVisible.TabIndex = 40;
            this.checkBox_objectVisible.Text = "Visible";
            this.checkBox_objectVisible.UseVisualStyleBackColor = true;
            this.checkBox_objectVisible.CheckStateChanged += new System.EventHandler(this.checkBox_objectVisible_CheckedChanged);
            // 
            // textBox_objectEventActive
            // 
            this.textBox_objectEventActive.Location = new System.Drawing.Point(134, 295);
            this.textBox_objectEventActive.Multiline = true;
            this.textBox_objectEventActive.Name = "textBox_objectEventActive";
            this.textBox_objectEventActive.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_objectEventActive.Size = new System.Drawing.Size(100, 57);
            this.textBox_objectEventActive.TabIndex = 40;
            // 
            // button_objectsMoveDown
            // 
            this.button_objectsMoveDown.Location = new System.Drawing.Point(62, 326);
            this.button_objectsMoveDown.Name = "button_objectsMoveDown";
            this.button_objectsMoveDown.Size = new System.Drawing.Size(61, 23);
            this.button_objectsMoveDown.TabIndex = 37;
            this.button_objectsMoveDown.Text = "Down";
            this.button_objectsMoveDown.UseVisualStyleBackColor = true;
            this.button_objectsMoveDown.Click += new System.EventHandler(this.button_objectsMoveDown_Click);
            // 
            // button_objectsMoveUp
            // 
            this.button_objectsMoveUp.Location = new System.Drawing.Point(5, 326);
            this.button_objectsMoveUp.Name = "button_objectsMoveUp";
            this.button_objectsMoveUp.Size = new System.Drawing.Size(51, 23);
            this.button_objectsMoveUp.TabIndex = 36;
            this.button_objectsMoveUp.Text = "Up";
            this.button_objectsMoveUp.UseVisualStyleBackColor = true;
            this.button_objectsMoveUp.Click += new System.EventHandler(this.button_objectsMoveUp_Click);
            // 
            // button_objectsRemove
            // 
            this.button_objectsRemove.Location = new System.Drawing.Point(62, 295);
            this.button_objectsRemove.Name = "button_objectsRemove";
            this.button_objectsRemove.Size = new System.Drawing.Size(61, 23);
            this.button_objectsRemove.TabIndex = 35;
            this.button_objectsRemove.Text = "Remove";
            this.button_objectsRemove.UseVisualStyleBackColor = true;
            this.button_objectsRemove.Click += new System.EventHandler(this.button_objectsRemove_Click);
            // 
            // button_objectsAdd
            // 
            this.button_objectsAdd.Location = new System.Drawing.Point(5, 295);
            this.button_objectsAdd.Name = "button_objectsAdd";
            this.button_objectsAdd.Size = new System.Drawing.Size(51, 23);
            this.button_objectsAdd.TabIndex = 34;
            this.button_objectsAdd.Text = "Add";
            this.button_objectsAdd.UseVisualStyleBackColor = true;
            this.button_objectsAdd.Click += new System.EventHandler(this.button_objectsAdd_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Objects List";
            // 
            // listBox_objects
            // 
            this.listBox_objects.FormattingEnabled = true;
            this.listBox_objects.Location = new System.Drawing.Point(7, 32);
            this.listBox_objects.Name = "listBox_objects";
            this.listBox_objects.Size = new System.Drawing.Size(120, 251);
            this.listBox_objects.TabIndex = 32;
            this.listBox_objects.SelectedIndexChanged += new System.EventHandler(this.listBox_objects_SelectedIndexChanged);
            // 
            // textBox_objectNext
            // 
            this.textBox_objectNext.Location = new System.Drawing.Point(238, 249);
            this.textBox_objectNext.Name = "textBox_objectNext";
            this.textBox_objectNext.Size = new System.Drawing.Size(196, 20);
            this.textBox_objectNext.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(130, 252);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(101, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Completed Location";
            // 
            // textBox_objectSoundEffect
            // 
            this.textBox_objectSoundEffect.Location = new System.Drawing.Point(334, 219);
            this.textBox_objectSoundEffect.Name = "textBox_objectSoundEffect";
            this.textBox_objectSoundEffect.Size = new System.Drawing.Size(100, 20);
            this.textBox_objectSoundEffect.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(331, 200);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 30;
            this.label17.Text = "Sound Effect";
            // 
            // textBox_objectAlpha
            // 
            this.textBox_objectAlpha.Location = new System.Drawing.Point(177, 197);
            this.textBox_objectAlpha.Name = "textBox_objectAlpha";
            this.textBox_objectAlpha.Size = new System.Drawing.Size(36, 20);
            this.textBox_objectAlpha.TabIndex = 29;
            this.textBox_objectAlpha.Leave += new System.EventHandler(this.textBox_objectAlpha_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(130, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Alpha";
            // 
            // textBox_objectBlend
            // 
            this.textBox_objectBlend.BackColor = System.Drawing.Color.White;
            this.textBox_objectBlend.Location = new System.Drawing.Point(270, 197);
            this.textBox_objectBlend.Name = "textBox_objectBlend";
            this.textBox_objectBlend.ReadOnly = true;
            this.textBox_objectBlend.Size = new System.Drawing.Size(36, 20);
            this.textBox_objectBlend.TabIndex = 27;
            this.textBox_objectBlend.TabStop = false;
            this.textBox_objectBlend.Click += new System.EventHandler(this.textBox_objectBlend_Click);
            this.textBox_objectBlend.Leave += new System.EventHandler(this.textBox_objectBlend_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(219, 200);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Blend";
            // 
            // textBox_objectWidth
            // 
            this.textBox_objectWidth.Location = new System.Drawing.Point(177, 171);
            this.textBox_objectWidth.Name = "textBox_objectWidth";
            this.textBox_objectWidth.Size = new System.Drawing.Size(36, 20);
            this.textBox_objectWidth.TabIndex = 25;
            this.textBox_objectWidth.Leave += new System.EventHandler(this.textBox_objectWidth_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(130, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Width";
            // 
            // textBox_objectHeight
            // 
            this.textBox_objectHeight.Location = new System.Drawing.Point(270, 171);
            this.textBox_objectHeight.Name = "textBox_objectHeight";
            this.textBox_objectHeight.Size = new System.Drawing.Size(36, 20);
            this.textBox_objectHeight.TabIndex = 23;
            this.textBox_objectHeight.Leave += new System.EventHandler(this.textBox_objectHeight_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(219, 174);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Height";
            // 
            // textBox_objectXOffset
            // 
            this.textBox_objectXOffset.Location = new System.Drawing.Point(177, 145);
            this.textBox_objectXOffset.Name = "textBox_objectXOffset";
            this.textBox_objectXOffset.Size = new System.Drawing.Size(36, 20);
            this.textBox_objectXOffset.TabIndex = 21;
            this.textBox_objectXOffset.Leave += new System.EventHandler(this.textBox_objectXOffset_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(130, 148);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "X Offset";
            // 
            // textBox_objectYOffset
            // 
            this.textBox_objectYOffset.Location = new System.Drawing.Point(270, 145);
            this.textBox_objectYOffset.Name = "textBox_objectYOffset";
            this.textBox_objectYOffset.Size = new System.Drawing.Size(36, 20);
            this.textBox_objectYOffset.TabIndex = 19;
            this.textBox_objectYOffset.Leave += new System.EventHandler(this.textBox_objectYOffset_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(219, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Y Offset";
            // 
            // textBox_objectDesc
            // 
            this.textBox_objectDesc.Location = new System.Drawing.Point(133, 60);
            this.textBox_objectDesc.Multiline = true;
            this.textBox_objectDesc.Name = "textBox_objectDesc";
            this.textBox_objectDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_objectDesc.Size = new System.Drawing.Size(176, 72);
            this.textBox_objectDesc.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(130, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Description";
            // 
            // textBox_objectTitle
            // 
            this.textBox_objectTitle.Location = new System.Drawing.Point(163, 15);
            this.textBox_objectTitle.Name = "textBox_objectTitle";
            this.textBox_objectTitle.Size = new System.Drawing.Size(146, 20);
            this.textBox_objectTitle.TabIndex = 10;
            this.textBox_objectTitle.Leave += new System.EventHandler(this.textBox_objectTitle_Leave);
            // 
            // textBox_objectFilename
            // 
            this.textBox_objectFilename.Location = new System.Drawing.Point(315, 151);
            this.textBox_objectFilename.Name = "textBox_objectFilename";
            this.textBox_objectFilename.ReadOnly = true;
            this.textBox_objectFilename.Size = new System.Drawing.Size(119, 20);
            this.textBox_objectFilename.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(130, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Title";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(312, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Filename";
            // 
            // button_objectClear
            // 
            this.button_objectClear.Location = new System.Drawing.Point(381, 174);
            this.button_objectClear.Name = "button_objectClear";
            this.button_objectClear.Size = new System.Drawing.Size(53, 23);
            this.button_objectClear.TabIndex = 9;
            this.button_objectClear.Text = "Clear";
            this.button_objectClear.UseVisualStyleBackColor = true;
            this.button_objectClear.Click += new System.EventHandler(this.button_objectClear_Click);
            // 
            // button_objectSelect
            // 
            this.button_objectSelect.Location = new System.Drawing.Point(315, 174);
            this.button_objectSelect.Name = "button_objectSelect";
            this.button_objectSelect.Size = new System.Drawing.Size(60, 23);
            this.button_objectSelect.TabIndex = 9;
            this.button_objectSelect.Text = "Select";
            this.button_objectSelect.UseVisualStyleBackColor = true;
            this.button_objectSelect.Click += new System.EventHandler(this.button_objectSelect_Click);
            // 
            // pictureBox_objectPreview
            // 
            this.pictureBox_objectPreview.Location = new System.Drawing.Point(315, 7);
            this.pictureBox_objectPreview.Name = "pictureBox_objectPreview";
            this.pictureBox_objectPreview.Size = new System.Drawing.Size(125, 125);
            this.pictureBox_objectPreview.TabIndex = 9;
            this.pictureBox_objectPreview.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(132, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Layer Properties";
            // 
            // checkBox_layerCompiled
            // 
            this.checkBox_layerCompiled.AutoSize = true;
            this.checkBox_layerCompiled.Location = new System.Drawing.Point(134, 104);
            this.checkBox_layerCompiled.Name = "checkBox_layerCompiled";
            this.checkBox_layerCompiled.Size = new System.Drawing.Size(69, 17);
            this.checkBox_layerCompiled.TabIndex = 12;
            this.checkBox_layerCompiled.Text = "Compiled";
            this.checkBox_layerCompiled.UseVisualStyleBackColor = true;
            // 
            // checkBox_layerLocked
            // 
            this.checkBox_layerLocked.AutoSize = true;
            this.checkBox_layerLocked.Location = new System.Drawing.Point(134, 81);
            this.checkBox_layerLocked.Name = "checkBox_layerLocked";
            this.checkBox_layerLocked.Size = new System.Drawing.Size(62, 17);
            this.checkBox_layerLocked.TabIndex = 11;
            this.checkBox_layerLocked.Text = "Locked";
            this.checkBox_layerLocked.UseVisualStyleBackColor = true;
            this.checkBox_layerLocked.CheckStateChanged += new System.EventHandler(this.checkBox_layerLocked_CheckedChanged);
            // 
            // checkBox_layerVisible
            // 
            this.checkBox_layerVisible.AutoSize = true;
            this.checkBox_layerVisible.Location = new System.Drawing.Point(134, 58);
            this.checkBox_layerVisible.Name = "checkBox_layerVisible";
            this.checkBox_layerVisible.Size = new System.Drawing.Size(56, 17);
            this.checkBox_layerVisible.TabIndex = 10;
            this.checkBox_layerVisible.Text = "Visible";
            this.checkBox_layerVisible.UseVisualStyleBackColor = true;
            this.checkBox_layerVisible.CheckStateChanged += new System.EventHandler(this.checkBox_layerVisible_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(376, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Events Inactive";
            // 
            // textBox_layerEventFalse
            // 
            this.textBox_layerEventFalse.Location = new System.Drawing.Point(379, 32);
            this.textBox_layerEventFalse.Multiline = true;
            this.textBox_layerEventFalse.Name = "textBox_layerEventFalse";
            this.textBox_layerEventFalse.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_layerEventFalse.Size = new System.Drawing.Size(100, 86);
            this.textBox_layerEventFalse.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(261, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Events Active";
            // 
            // textBox_layerEvent
            // 
            this.textBox_layerEvent.Location = new System.Drawing.Point(264, 32);
            this.textBox_layerEvent.Multiline = true;
            this.textBox_layerEvent.Name = "textBox_layerEvent";
            this.textBox_layerEvent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_layerEvent.Size = new System.Drawing.Size(100, 86);
            this.textBox_layerEvent.TabIndex = 6;
            // 
            // button_layerMoveDown
            // 
            this.button_layerMoveDown.Location = new System.Drawing.Point(65, 459);
            this.button_layerMoveDown.Name = "button_layerMoveDown";
            this.button_layerMoveDown.Size = new System.Drawing.Size(61, 23);
            this.button_layerMoveDown.TabIndex = 5;
            this.button_layerMoveDown.Text = "Down";
            this.button_layerMoveDown.UseVisualStyleBackColor = true;
            this.button_layerMoveDown.Click += new System.EventHandler(this.button_layerMoveDown_Click);
            // 
            // button_layerMoveUp
            // 
            this.button_layerMoveUp.Location = new System.Drawing.Point(8, 459);
            this.button_layerMoveUp.Name = "button_layerMoveUp";
            this.button_layerMoveUp.Size = new System.Drawing.Size(51, 23);
            this.button_layerMoveUp.TabIndex = 4;
            this.button_layerMoveUp.Text = "Up";
            this.button_layerMoveUp.UseVisualStyleBackColor = true;
            this.button_layerMoveUp.Click += new System.EventHandler(this.button_layerMoveUp_Click);
            // 
            // button2_layerRemove
            // 
            this.button2_layerRemove.Location = new System.Drawing.Point(65, 428);
            this.button2_layerRemove.Name = "button2_layerRemove";
            this.button2_layerRemove.Size = new System.Drawing.Size(61, 23);
            this.button2_layerRemove.TabIndex = 3;
            this.button2_layerRemove.Text = "Remove";
            this.button2_layerRemove.UseVisualStyleBackColor = true;
            this.button2_layerRemove.Click += new System.EventHandler(this.button2_layerRemove_Click);
            // 
            // button_layerAdd
            // 
            this.button_layerAdd.Location = new System.Drawing.Point(8, 428);
            this.button_layerAdd.Name = "button_layerAdd";
            this.button_layerAdd.Size = new System.Drawing.Size(51, 23);
            this.button_layerAdd.TabIndex = 2;
            this.button_layerAdd.Text = "Add";
            this.button_layerAdd.UseVisualStyleBackColor = true;
            this.button_layerAdd.Click += new System.EventHandler(this.button_layerAdd_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Layers";
            // 
            // listBox_layers
            // 
            this.listBox_layers.FormattingEnabled = true;
            this.listBox_layers.Location = new System.Drawing.Point(6, 32);
            this.listBox_layers.Name = "listBox_layers";
            this.listBox_layers.Size = new System.Drawing.Size(120, 381);
            this.listBox_layers.TabIndex = 0;
            this.listBox_layers.SelectedIndexChanged += new System.EventHandler(this.listBox_layers_SelectedIndexChanged);
            // 
            // openFileDialog_objImage
            // 
            this.openFileDialog_objImage.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_objImage_FileOk);
            // 
            // button_IOSave
            // 
            this.button_IOSave.Location = new System.Drawing.Point(572, 627);
            this.button_IOSave.Name = "button_IOSave";
            this.button_IOSave.Size = new System.Drawing.Size(75, 23);
            this.button_IOSave.TabIndex = 3;
            this.button_IOSave.Text = "Save";
            this.button_IOSave.UseVisualStyleBackColor = true;
            this.button_IOSave.Click += new System.EventHandler(this.button_IOSave_Click);
            // 
            // button_IOLoad
            // 
            this.button_IOLoad.Location = new System.Drawing.Point(653, 627);
            this.button_IOLoad.Name = "button_IOLoad";
            this.button_IOLoad.Size = new System.Drawing.Size(75, 23);
            this.button_IOLoad.TabIndex = 4;
            this.button_IOLoad.Text = "Load";
            this.button_IOLoad.UseVisualStyleBackColor = true;
            this.button_IOLoad.Click += new System.EventHandler(this.button_IOLoad_Click);
            // 
            // button_IOCompile
            // 
            this.button_IOCompile.Location = new System.Drawing.Point(734, 627);
            this.button_IOCompile.Name = "button_IOCompile";
            this.button_IOCompile.Size = new System.Drawing.Size(75, 23);
            this.button_IOCompile.TabIndex = 5;
            this.button_IOCompile.Text = "Export";
            this.button_IOCompile.UseVisualStyleBackColor = true;
            this.button_IOCompile.Click += new System.EventHandler(this.button_IOCompile_Click);
            // 
            // saveFileDialog_export
            // 
            this.saveFileDialog_export.DefaultExt = "*.json";
            this.saveFileDialog_export.Filter = "JSON Files|*.json";
            this.saveFileDialog_export.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_export_FileOk);
            // 
            // saveFileDialog_save
            // 
            this.saveFileDialog_save.DefaultExt = "*.JSON";
            this.saveFileDialog_save.Filter = "JSON Files|*.JSON";
            this.saveFileDialog_save.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_save_FileOk);
            // 
            // openFileDialog_load
            // 
            this.openFileDialog_load.DefaultExt = "*.JSON";
            this.openFileDialog_load.Filter = "JSON Files|*.JSON";
            this.openFileDialog_load.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_load_FileOk);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 600);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseUp);
            // 
            // checkBox_objectTooltip
            // 
            this.checkBox_objectTooltip.AutoSize = true;
            this.checkBox_objectTooltip.Location = new System.Drawing.Point(191, 40);
            this.checkBox_objectTooltip.Name = "checkBox_objectTooltip";
            this.checkBox_objectTooltip.Size = new System.Drawing.Size(94, 17);
            this.checkBox_objectTooltip.TabIndex = 46;
            this.checkBox_objectTooltip.Text = "Show Tooltip?";
            this.checkBox_objectTooltip.UseVisualStyleBackColor = true;
            // 
            // textBox_backgroundTitle
            // 
            this.textBox_backgroundTitle.Location = new System.Drawing.Point(267, 78);
            this.textBox_backgroundTitle.Name = "textBox_backgroundTitle";
            this.textBox_backgroundTitle.Size = new System.Drawing.Size(189, 20);
            this.textBox_backgroundTitle.TabIndex = 43;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(212, 81);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "Title";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1416, 702);
            this.Controls.Add(this.button_IOCompile);
            this.Controls.Add(this.button_IOLoad);
            this.Controls.Add(this.button_IOSave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "P&C Builder";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_backgoundImg)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_objectPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_backgoundSelectFile;
        private System.Windows.Forms.PictureBox pictureBox_backgoundImg;
        private System.Windows.Forms.TextBox textBox_backgoundFilename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_backgoundClear;
        private System.Windows.Forms.OpenFileDialog openFileDialog_backgroundImg;
        private System.Windows.Forms.TextBox textBox_backgroundCompletedLocation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_backgroundMusic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox_objectNext;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox_objectSoundEffect;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox_objectAlpha;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox_objectBlend;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox_objectWidth;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_objectHeight;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox_objectXOffset;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_objectYOffset;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_objectDesc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_objectTitle;
        private System.Windows.Forms.TextBox textBox_objectFilename;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button_objectClear;
        private System.Windows.Forms.Button button_objectSelect;
        private System.Windows.Forms.PictureBox pictureBox_objectPreview;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox_layerCompiled;
        private System.Windows.Forms.CheckBox checkBox_layerLocked;
        private System.Windows.Forms.CheckBox checkBox_layerVisible;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_layerEventFalse;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_layerEvent;
        private System.Windows.Forms.Button button_layerMoveDown;
        private System.Windows.Forms.Button button_layerMoveUp;
        private System.Windows.Forms.Button button2_layerRemove;
        private System.Windows.Forms.Button button_layerAdd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox_layers;
        private System.Windows.Forms.Button button_objectsMoveDown;
        private System.Windows.Forms.Button button_objectsMoveUp;
        private System.Windows.Forms.Button button_objectsRemove;
        private System.Windows.Forms.Button button_objectsAdd;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ListBox listBox_objects;
        private System.Windows.Forms.TextBox textbox_layerName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox checkBox_objectCompiled;
        private System.Windows.Forms.CheckBox checkBox_objectLocked;
        private System.Windows.Forms.CheckBox checkBox_objectVisible;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox_objectEventInactive;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox_objectEventActive;
        private System.Windows.Forms.OpenFileDialog openFileDialog_objImage;
        private System.Windows.Forms.ColorDialog colorDialog;
        private NewPanel panel1;
        private System.Windows.Forms.TextBox textBox_objectCost;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button_IOSave;
        private System.Windows.Forms.Button button_IOLoad;
        private System.Windows.Forms.Button button_IOCompile;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_export;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_save;
        private System.Windows.Forms.OpenFileDialog openFileDialog_load;
        private System.Windows.Forms.TextBox textBox_backgroundLoop;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox_backgroundRandomSounds;
        private System.Windows.Forms.CheckBox checkBox_objectTooltip;
        private System.Windows.Forms.TextBox textBox_backgroundTitle;
        private System.Windows.Forms.Label label26;
    }
}

